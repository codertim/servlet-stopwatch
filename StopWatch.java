

import java.io.*;
import java.util.Date;
import java.util.Calendar;
import java.util.Enumeration;

import javax.servlet.*;
import javax.servlet.http.*;


public class StopWatch extends HttpServlet {
	final static String BACKGROUND_COLOR = "#333333";
	final static String FOREGROUND_COLOR = "yellow";
	final static String DARK_YELLOW      = "#ffd800";
	final static boolean DEBUG           = false;

	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		boolean isDone            = false;
		String baseDateTimeNumber = request.getParameter("base_date_time_number");
		String baseInterval       = request.getParameter("base_interval");
		String newInterval        = request.getParameter("new_interval");

		System.out.println("##### doGet - baseDateTimeNumber = |" + baseDateTimeNumber + "|");
		System.out.println("##### doGet - baseInterval = |"       + baseInterval + "|");
		System.out.println("##### doGet - newInterval= |"         + newInterval+ "|");

		showMisc(request);

		if( (baseInterval == null) || (baseInterval.trim().equals("")) ) {
			System.out.println("##### doGet - baseInterval hidden param is empty");
			baseInterval = "";
		}

		if( (newInterval != null) && (!newInterval.trim().equals("")) ) {
			baseInterval = newInterval;
			long baseDateTimeLong = (new java.util.Date()).getTime();
			baseDateTimeNumber = String.valueOf(baseDateTimeLong);
			System.out.println("##### doGet - baseInterval assigned newInterval=" + newInterval);
		}

		//if( (baseDateTimeNumber == null) || (baseDateTimeNumber.trim().equals("")) ) {
		//	long baseDateTimeLong = (new java.util.Date()).getTime();
		//	baseDateTimeNumber = String.valueOf(baseDateTimeLong);
		//}

		java.util.Date currentDate = new java.util.Date();

		if( (baseDateTimeNumber != null) && (!"null".equals(baseDateTimeNumber)) )  {
			Long baseDateLong = new Long(baseDateTimeNumber);
			Date baseDate = new Date(baseDateLong.longValue());
			Calendar currentCalendar = Calendar.getInstance();
			Calendar targetCalendar  = Calendar.getInstance();
			currentCalendar.setTime(currentDate);
			targetCalendar.setTime(baseDate);
			Integer intervalInteger = new Integer(baseInterval);
			targetCalendar.add(Calendar.MINUTE, intervalInteger.intValue());
			System.out.println("current calendar = " + currentCalendar);
			System.out.println("target calendar = " + targetCalendar);

			if(currentCalendar.after(targetCalendar)) {
				isDone = true;
				System.out.println("Done");
			}
		}

		Cookie cookie = new Cookie("mycookie", currentDate.toString());
		response.addCookie(cookie);
		showCookies(request);

		doView(response, currentDate, baseInterval, baseDateTimeNumber, isDone);
	}



	private void doJavaScript(PrintWriter out) {
		out.println("<script language='JavaScript'>");
		out.println("    function refreshPage() {");
		out.println("	     document.forms[0].submit();");
		out.println("	     // alert('Hello world');");
		out.println("    }");
		out.println("    setTimeout('refreshPage()', 5000);");
		out.println("</script>");
	}



	private void showCookies(HttpServletRequest req) {
		Cookie[] cookies = req.getCookies();

		if ((cookies == null) || (cookies.length == 0)) {
			System.out.println("No cookies found");
		} else {
			Cookie cookie = null;
			for(int i=0; i < cookies.length; i++) {
				cookie = cookies[i];
				System.out.println("  Current cookie: " + cookie.getName());
			}
		}
	}



	private void doView(HttpServletResponse response, java.util.Date currentDate, String baseInterval, String baseDateTimeNumber, boolean isDone) {
		try {
			PrintWriter out = response.getWriter();
			out.println("<html>");
			out.println("    <head>");
			doCss(out);
			doJavaScript(out);
			out.println("    </head>");
			out.println("    <body style=\"background-color: " + BACKGROUND_COLOR + "; color:yellow\">");
			out.println("        <div style='margin: 0 auto; width: 300px'>");
			out.println("            <h1>Stopwatch</h1>");
			out.println("        </div>");


			out.println("        <div style=\"margin:100px 200px; padding:50px\">");

			if(isDone) {
				out.println("<div style='color:red; font-size: 3em;'>Done!!!</div> <br /><br />");
			}
			out.println("Current Time = " + currentDate);
			out.println("<form action=\"StopWatch\">");
			out.println("    <input type='hidden' name='base_interval' value='" + baseInterval + "' />");
			out.println("    <input type='hidden' name='base_date_time_number' value='" + baseDateTimeNumber + "' />");

			if(DEBUG) {
				out.println("         <br />DEBUGGING: base interval = " + baseInterval + "<br /><br />");
			}

			if( (baseInterval != null) && (baseInterval.length() > 0) ) {
				out.println("         <br /><br />Minutes: " + baseInterval + " <br /><br />");
			}
	
			if( (baseDateTimeNumber != null) && (!("".equals(baseDateTimeNumber)) && (!("null".equals(baseDateTimeNumber))) ) ) {
				Long baseDateTimeNumberLong = new Long(baseDateTimeNumber);
				out.println("         <br />Start time= " + (new java.util.Date(baseDateTimeNumberLong.longValue())) + "<br /><br />");
				if(DEBUG) {
					out.println("         <br />DEBUGGING: base date time= " + (new java.util.Date(baseDateTimeNumberLong.longValue())) + "<br /><br />");
				}

			}
			/* else {
				out.println("         <br />Start time= NULL <br /><br />");
				if(DEBUG) {
					out.println("         <br />DEBUGGING: base date time= NULL <br /><br />");
				}
			}
			*/

			if( (baseInterval == null) || (baseInterval.length() == 0) ) {
				out.println("    <br /><br />");
				out.println("    Enter minutes: <br /><br />");
				out.println("    <input type='text' name='new_interval' />");
				out.println("    <br /><br />");
				out.println("    <input type='submit' value='Go' />");
			}
			out.println("</form>");

			out.println("        </div>");
			out.println("    </body>");
			out.println("</html>");

		} catch(IOException e) {
			System.out.println("##### doView - ERROR - problem with response writer - error message = " + e.getMessage());
		}

	}


	private void doCss(PrintWriter out) {
		out.println("<style>");
		out.println("    input[type='submit'] { background-color:" + FOREGROUND_COLOR + " ; border-color: " + DARK_YELLOW + "; border-width: 5px 0; border-style:solid; color: " + BACKGROUND_COLOR + "; font-size: 1.5em; font-style: italic; font-weight: bold; padding: 1px 70px; } ");
		out.println("</style>");
	}



	private void showMisc(HttpServletRequest request) {
		System.out.println("##### showMisc#misc - REQUEST_METHOD=" + request.getMethod());
		System.out.println("##### showMisc#misc - QUERY_STRING=" + request.getQueryString());
		System.out.println("##### showMisc#misc - DOCUMENT_ROOT=" + getServletContext().getRealPath("/"));

		// explort ServletContext
		ServletContext servletContext = getServletContext();
		Enumeration initParameterNames = servletContext.getInitParameterNames();
		if(initParameterNames.hasMoreElements()) {
			while(initParameterNames.hasMoreElements()) {
				String parameterName = (String) initParameterNames.nextElement();
				System.out.println("*** ServletContext parameter name = " + parameterName);
			}
		} else {
				System.out.println("*** ServletContext - no parameters found");
		}
	}

}


